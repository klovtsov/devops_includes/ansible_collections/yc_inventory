#!/usr/bin/env python3
# Make coding more python3-ish, this is required for contributions to Ansible
from __future__ import absolute_import, division, print_function

__metaclass__ = type

DOCUMENTATION = """
    name: klovtsov.yc_inventory.yc_compute
    plugin_type: inventory
    short_description: Yandex Cloud compute inventory source
    requirements:
        - yandexcloud
    extends_documentation_fragment:
        - inventory_cache
        - constructed
    description:
        - Get inventory hosts from Yandex Cloud
        - Uses a YAML configuration file that ends with C(yc_compute.(yml|yaml)).
    options:
        plugin:
            description: Token that ensures this is a source file for the plugin.
            required: True
            choices: ['klovtsov.yc_inventory.yc_compute']
        yc_token:
            description: Token for yandex cloud connection
        yc_token_file:
            description: File with token for yandex cloud connection
        yc_service_account_key_file:
            description: File with service account key for yandex cloud connection
        yc_clouds:
            description: Names of clouds to get hosts from
            type: list
            default: []
        yc_folders:
            description: Names of folders to get hosts from
            type: list
            default: []
        yc_group_label:
            description: VM's label used for group assignment
            type: string
            default: ""
"""

import json
import os

from ansible.errors import AnsibleError
from ansible.plugins.inventory import BaseInventoryPlugin, Constructable, Cacheable
from ansible.module_utils._text import to_native

try:
    import yandexcloud
    from yandex.cloud.compute.v1.instance_service_pb2 import ListInstancesRequest
    from yandex.cloud.compute.v1.instance_service_pb2_grpc import InstanceServiceStub
    from google.protobuf.json_format import MessageToDict
    from yandex.cloud.resourcemanager.v1.cloud_service_pb2 import ListCloudsRequest
    from yandex.cloud.resourcemanager.v1.cloud_service_pb2_grpc import CloudServiceStub
    from yandex.cloud.resourcemanager.v1.folder_service_pb2 import ListFoldersRequest
    from yandex.cloud.resourcemanager.v1.folder_service_pb2_grpc import (
        FolderServiceStub,
    )
except ImportError:
    raise AnsibleError("Yandex cloud dynamic inventory plugin requires yandexcloud")


class InventoryModule(BaseInventoryPlugin, Constructable, Cacheable):
    NAME = "klovtsov.yc_inventory.yc_compute"  # used internally by Ansible, it should match the file name but not required

    def verify_file(self, path):
        if super(InventoryModule, self).verify_file(path):
            if path.endswith(("yc_compute.yml", "yc_compute.yaml")):
                return True
        return False

    def _get_ip_for_instance(self, instance):
        interfaces = instance["networkInterfaces"]
        for interface in interfaces:
            address = interface["primaryV4Address"]
            if address:
                if address.get("oneToOneNat"):
                    return address["oneToOneNat"]["address"]
                else:
                    return address["address"]
        return None

    def _init_client(self):
        yc_sa_key_file = self.get_option("yc_service_account_key_file")
        yc_token_file = self.get_option("yc_token_file")

        yc_sa_key_file_valid = yc_sa_key_file is not None and os.path.isfile(
            yc_sa_key_file
        )
        yc_token_file_valid = yc_token_file is not None and os.path.isfile(
            yc_token_file
        )

        if yc_sa_key_file_valid:
            with open(yc_sa_key_file) as fhand:
                yc_sa_key = json.load(fhand)
        if yc_token_file_valid:
            with open(yc_token_file) as fhand:
                token = fhand.read().strip()
        else:
            token = self.get_option("yc_token")

        if not token and not yc_sa_key:
            raise AnsibleError(
                "Provide yc_service_account_key_file or yc_token_file or yc_token"
            )

        yc_sdk = yandexcloud.SDK(iam_token=token, service_account_key=yc_sa_key)
        self.instance_service = yc_sdk.client(InstanceServiceStub)
        self.folder_service = yc_sdk.client(FolderServiceStub)
        self.cloud_service = yc_sdk.client(CloudServiceStub)

    def _get_clouds(self):
        all_clouds = MessageToDict(self.cloud_service.List(ListCloudsRequest()))[
            "clouds"
        ]
        if self.get_option("yc_clouds"):
            all_clouds[:] = [
                cloud
                for cloud in all_clouds
                if cloud["name"] in self.get_option("yc_clouds")
            ]
        self.clouds = all_clouds

    def _get_folders(self):
        all_folders = []
        for cloud in self.clouds:
            all_folders += MessageToDict(
                self.folder_service.List(ListFoldersRequest(cloud_id=cloud["id"]))
            )["folders"]

        if self.get_option("yc_folders"):
            all_folders[:] = [
                folder
                for folder in all_folders
                if folder["name"] in self.get_option("yc_folders")
            ]

        self.folders = all_folders

    def _get_all_hosts(self):
        self.hosts = []
        for folder in self.folders:
            hosts = self.instance_service.List(
                ListInstancesRequest(folder_id=folder["id"])
            )
            dict_ = MessageToDict(hosts)

            if dict_:
                self.hosts += dict_["instances"]

    def _process_hosts(self):
        group_label = str(self.get_option("yc_group_label"))

        for instance in self.hosts:
            if group_label and group_label in instance["labels"]:
                group = instance["labels"][group_label]
            else:
                group = "ycloud"

            self.inventory.add_group(group=group)
            if instance["status"] == "RUNNING":
                ip = self._get_ip_for_instance(instance)
                if ip:
                    self.inventory.add_host(instance["name"], group=group)
                    self.inventory.set_variable(
                        instance["name"], "ansible_host", to_native(ip)
                    )

    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path)

        self._read_config_data(path)
        self._init_client()

        self._get_clouds()
        self._get_folders()

        self._get_all_hosts()
        self._process_hosts()
